package com.neosoft.Microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MicroServiceWithApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServiceWithApiGatewayApplication.class, args);
	}

}
